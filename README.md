## PMEM Calculator
When emulating pmem one must identify [usable memory regions](https://nvdimm.wiki.kernel.org/how_to_choose_the_correct_memmap_kernel_parameter_for_pmem_on_your_system) and construct proper memmap parameters. This is a simple tool to help construct these for you.

### Usage
```
./pmem.sh
```

### Lumo
This script requires [Lumo](https://github.com/anmonteiro/lumo). You'll need to update the shebang line of calc.cljs for your system.

### Sample Output
```
$ ./pmem.sh
size[mb]            memmap
0.6249990463256836  0x000000000009ffff!0x0000000000000000
1.1249990463256836  0x000000000011ffff!0x00000000ca967000
2.4335927963256836  0x000000000026efff!0x00000000ca4db000
509.9999990463257   0x000000001fdfffff!0x0000000020200000
510.9999990463257   0x000000001fefffff!0x0000000000100000
2210.5937490463257  0x000000008a297fff!0x0000000040200000
4861.999999046326   0x000000012fdfffff!0x0000000100000000
```
