#!/home/wes/bin/lumo
(require '[clojure.string :as str]
         '[lumo.io :as io])

;; sync wait on stdin
(def input (io/slurp "/dev/stdin"))

(defn hex->dec
  "Convert hex string to decimal integer"
  [hexnum]
  (js/parseInt hexnum 16))

(defn dec->hex
  "Convert decimal integer or string to hex string"
  [decnum]
  (let [hexnum  (.toString decnum 16)
        len     (count hexnum)
        pad     (- 16 len)
        padding (apply str (take pad (repeat "0")))
        output (str "0x" padding hexnum)]
    output))

(defn process-input
  "Process input and return list of [starting-hex ending-hex]"
  [inp]
  (map #(str/split % #"-")
       (str/split-lines inp)))

(defn calc-mem-size
  "Calculate size of memory region given by memrow. Produce result row."
  [memrow]
  (let [[start end]   memrow 
        size          (- (hex->dec end)
                         (hex->dec start))
        sizehex       (dec->hex size)
        sizemb        (/ size (* 1024 1024))
        memmap        (str sizehex "!" start)]
    [sizemb memmap]))

;; "main"
(->> input
     process-input
     (map calc-mem-size)
     (sort-by first)
     (cons ["size[mb]" "memmap"])
     (map #(apply str (str/join " " %))) ;; formatting
     (str/join "\n") ;; formatting
     println)
