#!/bin/bash
dmesg | grep BIOS-e820       \
      | grep usable          \
      | grep -Po '\[mem.+\]' \
      | cut -d' ' -f 2       \
      | tr -d ']'            \
